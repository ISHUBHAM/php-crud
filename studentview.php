<?php

include('config.php');

?>

<!DOCTYPE html>
<html>
<head>
	<title>Student-View</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>





	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<table class="table table-striped table-dark">
					<thead>

						<tr>
							<th scope="col">Type</th>
							<th scope="col">Name</th>
							<th scope="col">Email</th>
							<th scope="col">Contact</th>
							<th scope="col">Subject</th>




						</tr>
						<?php
						$sql = 'SELECT user_id,name,email,contact,subject,type FROM user a INNER JOIN student b on a.id = b.user_id INNER JOIN class c on b.id=c.student_id INNER JOIN subject d on b.id = d.student_id';

						$stmtinsert = $db->prepare($sql);
						$result=$stmtinsert->execute();

						while ($result = $stmtinsert->fetch(PDO::FETCH_ASSOC)) 
							{ ?>
								<tr>
									<td><?php echo $result['type'] ?> </td>
									<td><?php echo  $result['name']?> </td>
									<td> <?php echo  $result['email']?> </td>
									<td>  <?php echo  $result['contact']?> </td>
									<td> <?php  echo  $result['subject']?> </td>
									<td><a>edit</a></td>
									<td><a href="/php-project/deleteuser.php?id=<?php echo $result['user_id'] ?>" class="btn btn-delete">delete</a></td>
								</tr>

								<?php
							}

							?>
						</thead>

					</table>

				</div>


			</div>
		</div>




	</body>
	</html>
<?php

include('config.php');

?>

<!DOCTYPE html>
<html>
<head>
	<title>Teacher-Form</title>


	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


</head>
<body>
  <div>
    <?php


    if (isset($_POST['create']))
    {

     $name    =$_POST['name']; 
     $contact =$_POST['contact'];
     $post    =$_POST['post'];
     $date    =$_POST['date'];





     $sql ="INSERT INTO user(type) VALUES(?)";
     $stmtinsert = $db->prepare($sql);
     $result=$stmtinsert->execute(['teacher']);


     $sql1 = $db->prepare("SELECT id  FROM user ORDER BY id DESC LIMIT 1");
     $sql1->execute(['teacher']);
     while ($result1 = $sql1->fetch(PDO::FETCH_ASSOC)) 
     {
      $teacherid= $result1['id']."<br/>";
    }




    $sql="INSERT INTO teacher(user_id,name,contact,post,created_at) VALUES(?,?,?,?,?)";
    $stmtinsert = $db->prepare($sql);
    $result=$stmtinsert->execute([$teacherid,$name,$contact,$post,$date]);

  }
  ?>



</div>


<div>
	<form action="teacheregis.php" method="post">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
         <h1>Teacher-Form</h1>
         <hr class="mb-3">

         <label for="name"><b>Name</b></label>
         <input type="text"  class="form-control" name="name" required>

         <label for="contact"><b>Contact</b></label>
         <input type="number"class="form-control" name="contact" required>

         <label for="post"><b>Post</b></label>
         <input type="text"  class="form-control" name="post" required>

         <label for="post"><b>Date</b></label>
         <input type="date"  class="form-control" name="date" required>

         <hr class="mb-3">

         <input  class="btn btn-primary" type="submit" name="create" value="Sign Up">

       </div>
     </div>
   </div>

 </form>
</div>


</body>
</html>
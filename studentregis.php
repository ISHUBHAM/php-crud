<?php

include('config.php');

?>

<!DOCTYPE html>
<html>
<head>
	<title>Student-Form</title>


	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>

	<div>
		<?php
		if (isset($_POST['create']))
		{

			$name    =$_POST['name'];
			$email   =$_POST['email'];
			$class   =$_POST['class'];
			$subject =$_POST['subject']; 
			$contact =$_POST['contact'];
			

			$sql ="INSERT INTO user(type) VALUES(?)";
			$stmtinsert = $db->prepare($sql);
			$result=$stmtinsert->execute(['student']);



             // $sql1="SELECT id FROM user ORDER BY id DESC LIMIT 1";  
             // $stmtinsert1 = $db->prepare($sql1);
             // $result1=$stmtinsert1->execute(['user']);

             //   $result1 =$stmtinsert1->setFetchMode(PDO::FETCH_ASSOC);
             // echo "$result1";

			$sql1 = $db->prepare("SELECT id  FROM user ORDER BY id DESC LIMIT 1");
			$sql1->execute(['student']);
			while ($result1 = $sql1->fetch(PDO::FETCH_ASSOC)) 
			{
				$username=$result1['id']."<br/>";
			}
             // fatch the recent data inserted in user table
			


			$sql="INSERT INTO student(user_id,name,email,class,contact) VALUES(?,?,?,?,?)";
			$stmtinsert = $db->prepare($sql);
			$result=$stmtinsert->execute([$username,$name,$email,$class,$contact]);
			





			$sql2 = $db->prepare("SELECT id  FROM student ORDER BY id DESC LIMIT 1");
			$sql2->execute(['subject']);
			while ($result2 = $sql2->fetch(PDO::FETCH_ASSOC)) 
			{
				$studentid= $result2['id']."<br/>";
			}




			$sql="INSERT INTO subject(student_id,subject) VALUES(?,?)";
			$stmtinsert = $db->prepare($sql);
			$result=$stmtinsert->execute([$studentid,$subject]);
              // echo "done";




			$sql="INSERT INTO class(student_id,class) VALUES(?,?)";
			$stmtinsert = $db->prepare($sql);
			$result=$stmtinsert->execute([$studentid,$class]);
              // echo "done";


            // if($result)
            // {
            //    echo "Succesfull ";

            // }
            // else
            // {
            //    echo "error save data";

            // }
			

		}

		?>
		
	</div>



	<div>
		<form action="studentregis.php" method="post">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<h1>Student-Registration-Form</h1>
						<hr class="mb-3">

						<label for="name"><b>Name</b></label>
						<input type="text"  class="form-control" name="name" required>

						<label for="email"><b>Email</b></label>
						<input type="email" class="form-control" name="email" required>

						<label for="class"><b>Class</b></label>
						<input type="text" class="form-control" name="class" required>

						<label for="subject"><b>Subject</b></label>
						<input type="text" class="form-control" name="subject" required>

						<label for="contact"><b>Contact</b></label>
						<input type="number"class="form-control" name="contact" required>
						<hr class="mb-3">

						<input  class="btn btn-primary" type="submit" name="create" value="Sign Up">

					</div>
				</div>
			</div>

		</form>
	</div>

</body>
</html>